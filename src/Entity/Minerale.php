<?php

namespace App\Entity;

use App\Repository\MineraleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MineraleRepository::class)
 * @Vich\Uploadable
 */
class Minerale
{
  
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $numero;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $nome;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $formula;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $accessori;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $geoLoc;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $geoProv;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $geoReg;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $geoNaz;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $geoGrup;

    /**
     * @ORM\ManyToOne(targetEntity=ClasseMineralogica::class, inversedBy="minerales")
     */
    private $classe;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $dimensione;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updateAt;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $dimensioneCristallo;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2, nullable=true)
     */
    private $peso;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $colore;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $collocazioneCampione;

    /**
     * @ORM\ManyToOne(targetEntity=Collocazione::class, inversedBy="minerales")
     */
    private $collocazione;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $fluo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $fluoDescrizione;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $matrice;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dataCollezione;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $note;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2, nullable=true)
     */
    private $prezzoVendita;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2, nullable=true)
     */
    private $valore;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $valoreData;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $condiviso = false;

    /**
     * @ORM\ManyToOne(targetEntity=Collezionista::class, inversedBy="minerales")
     */
    private $collezionista;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $data;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     * @var string
     */
    private $immagine;

    /**
     * @Vich\UploadableField(mapping="minerali_immagini", fileNameProperty="immagine")
     * @var File
     */
    private $immagineFile;

    /**
     * @ORM\OneToMany(targetEntity=Document::class, mappedBy="minerale", cascade={"persist","remove"}, orphanRemoval=true)
     */
    private $documents;

    public function __construct()
    {
        $this->documents = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id ? $this->nome : "";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(?string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(?string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getFormula(): ?string
    {
        return $this->formula;
    }

    public function setFormula(?string $formula): self
    {
        $this->formula = $formula;

        return $this;
    }

    public function getAccessori(): ?string
    {
        return $this->accessori;
    }

    public function setAccessori(?string $accessori): self
    {
        $this->accessori = $accessori;

        return $this;
    }

    public function getGeoLoc(): ?string
    {
        return $this->geoLoc;
    }

    public function setGeoLoc(?string $geoLoc): self
    {
        $this->geoLoc = $geoLoc;

        return $this;
    }

    public function getGeoProv(): ?string
    {
        return $this->geoProv;
    }

    public function setGeoProv(?string $geoProv): self
    {
        $this->geoProv = $geoProv;

        return $this;
    }

    public function getGeoReg(): ?string
    {
        return $this->geoReg;
    }

    public function setGeoReg(?string $geoReg): self
    {
        $this->geoReg = $geoReg;

        return $this;
    }

    public function getGeoNaz(): ?string
    {
        return $this->geoNaz;
    }

    public function setGeoNaz(?string $geoNaz): self
    {
        $this->geoNaz = $geoNaz;

        return $this;
    }

    public function getGeoGrup(): ?string
    {
        return $this->geoGrup;
    }

    public function setGeoGrup(?string $geoGrup): self
    {
        $this->geoGrup = $geoGrup;

        return $this;
    }

    public function getClasse(): ?ClasseMineralogica
    {
        return $this->classe;
    }

    public function setClasse(?ClasseMineralogica $classe): self
    {
        $this->classe = $classe;

        return $this;
    }

    public function getDimensione(): ?string
    {
        return $this->dimensione;
    }

    public function setDimensione(?string $dimensione): self
    {
        $this->dimensione = $dimensione;

        return $this;
    }

    public function getDimensioneCristallo(): ?string
    {
        return $this->dimensioneCristallo;
    }

    public function setDimensioneCristallo(?string $dimensioneCristallo): self
    {
        $this->dimensioneCristallo = $dimensioneCristallo;

        return $this;
    }

    public function getPeso(): ?string
    {
        return $this->peso;
    }

    public function setPeso(?string $peso): self
    {
        $this->peso = $peso;

        return $this;
    }

    public function getColore(): ?string
    {
        return $this->colore;
    }

    public function setColore(?string $colore): self
    {
        $this->colore = $colore;

        return $this;
    }

    public function getCollocazioneCampione(): ?string
    {
        return $this->collocazioneCampione;
    }

    public function setCollocazioneCampione(?string $collocazioneCampione): self
    {
        $this->collocazioneCampione = $collocazioneCampione;

        return $this;
    }

    public function getCollocazione(): ?Collocazione
    {
        return $this->collocazione;
    }

    public function setCollocazione(?Collocazione $collocazione): self
    {
        $this->collocazione = $collocazione;

        return $this;
    }

    public function getFluo(): ?int
    {
        return $this->fluo;
    }

    public function setFluo(?int $fluo): self
    {
        $this->fluo = $fluo;

        return $this;
    }

    public function getFluoDescrizione(): ?string
    {
        return $this->fluoDescrizione;
    }

    public function setFluoDescrizione(?string $fluoDescrizione): self
    {
        $this->fluoDescrizione = $fluoDescrizione;

        return $this;
    }

    public function getMatrice(): ?string
    {
        return $this->matrice;
    }

    public function setMatrice(?string $matrice): self
    {
        $this->matrice = $matrice;

        return $this;
    }

    public function getDataCollezione(): ?\DateTimeInterface
    {
        return $this->dataCollezione;
    }

    public function setDataCollezione(?\DateTimeInterface $dataCollezione): self
    {
        $this->dataCollezione = $dataCollezione;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getPrezzoVendita(): ?string
    {
        return $this->prezzoVendita;
    }

    public function setPrezzoVendita(?string $prezzoVendita): self
    {
        $this->prezzoVendita = $prezzoVendita;

        return $this;
    }

    public function getValore(): ?string
    {
        return $this->valore;
    }

    public function setValore(?string $valore): self
    {
        $this->valore = $valore;

        return $this;
    }

    public function getValoreData(): ?\DateTimeInterface
    {
        return $this->valoreData;
    }

    public function setValoreData(?\DateTimeInterface $valoreData): self
    {
        $this->valoreData = $valoreData;

        return $this;
    }

    public function getCondiviso(): ?bool
    {
        return $this->condiviso;
    }

    public function setCondiviso(?bool $condiviso): self
    {
        $this->condiviso = $condiviso;

        return $this;
    }

    public function getCollezionista(): ?Collezionista
    {
        return $this->collezionista;
    }

    public function setCollezionista(?Collezionista $collezionista): self
    {
        $this->collezionista = $collezionista;

        return $this;
    }

    public function getData(): ?\DateTimeInterface
    {
        return $this->data;
    }

    public function setData(?\DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function setImmagineFile(File $immagine = null)
    {
        $this->immagineFile = $immagine;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($immagine) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updateAt = new \DateTime('now');
        }
    }

    public function getImmagineFile()
    {
        return $this->immagineFile;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getImmagine(): ?string
    {
        return $this->immagine;
    }

    public function setImmagine(?string $immagine): self
    {
        $this->immagine = $immagine;

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setMinerale($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document)) {
            // set the owning side to null (unless already changed)
            if ($document->getMinerale() === $this) {
                $document->setMinerale(null);
            }
        }

        return $this;
    }

}
