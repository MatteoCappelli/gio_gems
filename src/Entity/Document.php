<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\DocumentRepository;

/**
 * @ORM\Entity(repositoryClass=DocumentRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Document
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $path;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mime;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateAdded;

    /**
     * @ORM\ManyToOne(targetEntity=Minerale::class, inversedBy="documents")
     */
    private $minerale;

    public function __toString(): ?string
    {
        return $this->fname;
    }

    /**
     * @ORM\PostRemove
     */
    public function postRemove() {
        // NOTE: getcwd() must be "/public" directory (as per default)
        $cwd = getcwd();
        $fpath = $cwd.'/uploads/'.$this->path;
        if (file_exists($fpath)) {
            unlink($fpath);
        }
        $pi = pathinfo($this->path);
        $fpath = $cwd.'/uploads/'.$pi['dirname'].'/.thumbs/'.$pi['basename'];
        if (file_exists($fpath)) {
            unlink($fpath);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFname(): ?string
    {
        return $this->fname;
    }

    public function setFname(string $fname): self
    {
        $this->fname = $fname;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getMime(): ?string
    {
        return $this->mime;
    }

    public function setMime(string $mime): self
    {
        $this->mime = $mime;

        return $this;
    }

    public function getDateAdded(): ?\DateTimeInterface
    {
        return $this->dateAdded;
    }

    public function setDateAdded(\DateTimeInterface $dateAdded): self
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

    public function getThumb() {
        $pi = pathinfo($this->path);

        return $pi['dirname'].'/.thumbs/'.$pi['basename'];
    }

    public function getMinerale(): ?Minerale
    {
        return $this->minerale;
    }

    public function setMinerale(?Minerale $minerale): self
    {
        $this->minerale = $minerale;

        return $this;
    }
}
