<?php

namespace App\Entity;

use App\Repository\CollocazioneRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CollocazioneRepository::class)
 */
class Collocazione
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $valore;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descrizione;

    /**
     * @ORM\OneToMany(targetEntity=Minerale::class, mappedBy="collocazione")
     */
    private $minerales;

    public function __construct()
    {
        $this->minerales = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id ? $this->valore : "";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValore(): ?string
    {
        return $this->valore;
    }

    public function setValore(?string $valore): self
    {
        $this->valore = $valore;

        return $this;
    }

    public function getDescrizione(): ?string
    {
        return $this->descrizione;
    }

    public function setDescrizione(?string $descrizione): self
    {
        $this->descrizione = $descrizione;

        return $this;
    }

    /**
     * @return Collection|Minerale[]
     */
    public function getMinerales(): Collection
    {
        return $this->minerales;
    }

    public function addMinerale(Minerale $minerale): self
    {
        if (!$this->minerales->contains($minerale)) {
            $this->minerales[] = $minerale;
            $minerale->setCollocazione($this);
        }

        return $this;
    }

    public function removeMinerale(Minerale $minerale): self
    {
        if ($this->minerales->removeElement($minerale)) {
            // set the owning side to null (unless already changed)
            if ($minerale->getCollocazione() === $this) {
                $minerale->setCollocazione(null);
            }
        }

        return $this;
    }
}
