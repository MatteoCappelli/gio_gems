<?php

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity=Collezionista::class, mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $collezionista;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $nome;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $cognome;

    public function __construct()
    {
      parent::__construct();
    }

    public function getCollezionista(): ?Collezionista
    {
        return $this->collezionista;
    }

    public function setCollezionista(?Collezionista $collezionista): self
    {
        $this->collezionista = $collezionista;

        // set (or unset) the owning side of the relation if necessary
        $newUser = null === $collezionista ? null : $this;
        if ($collezionista->getUser() !== $newUser) {
            $collezionista->setUser($newUser);
        }

        return $this;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(?string $nome): self
    {
        $this->nome = ucfirst($nome);

        return $this;
    }

    public function getCognome(): ?string
    {
        return $this->cognome;
    }

    public function setCognome(?string $cognome): self
    {
        $this->cognome = ucfirst($cognome);

        return $this;
    }

    public function getRolesStr(): string
    {
        return isset($this->roles[0])? $this->roles[0] : '';
    }

    public function setRolesStr(string $str): self
    {
        $this->roles[0] = $str;
        return $this;
    }

    public function getFullName(): string
    {
      return $this->getNome(). ' ' . $this->getCognome();
    }

    public function setEmail($email)
    {
      $email = is_null($email) ? '' : $email;
      parent::setEmail($email);
      $this->setUsername($email);

      return $this;
    }

}
