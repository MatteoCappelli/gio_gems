<?php

namespace App\Entity;

use App\Repository\CollezionistaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CollezionistaRepository::class)
 */
class Collezionista
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $nome;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $cognome;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="collezionista", cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Minerale::class, mappedBy="collezionista")
     */
    private $minerales;

    /**
     * @ORM\OneToOne(targetEntity=Impostazioni::class, mappedBy="collezionista", cascade={"persist", "remove"})
     */
    private $impostazioni;

    public function __construct()
    {
        $this->minerales = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id ? $this->getFullName() : "";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(?string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getCognome(): ?string
    {
        return $this->cognome;
    }

    public function setCognome(?string $cognome): self
    {
        $this->cognome = $cognome;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Minerale[]
     */
    public function getMinerales(): Collection
    {
        return $this->minerales;
    }

    public function addMinerale(Minerale $minerale): self
    {
        if (!$this->minerales->contains($minerale)) {
            $this->minerales[] = $minerale;
            $minerale->setCollezionista($this);
        }

        return $this;
    }

    public function removeMinerale(Minerale $minerale): self
    {
        if ($this->minerales->removeElement($minerale)) {
            // set the owning side to null (unless already changed)
            if ($minerale->getCollezionista() === $this) {
                $minerale->setCollezionista(null);
            }
        }

        return $this;
    }

    public function getFullName(): string
    {
      return $this->getNome(). ' ' . $this->getCognome();
    }

    public function getImpostazioni(): ?Impostazioni
    {
        return $this->impostazioni;
    }

    public function setImpostazioni(?Impostazioni $impostazioni): self
    {
        // unset the owning side of the relation if necessary
        if ($impostazioni === null && $this->impostazioni !== null) {
            $this->impostazioni->setCollezionista(null);
        }

        // set the owning side of the relation if necessary
        if ($impostazioni !== null && $impostazioni->getCollezionista() !== $this) {
            $impostazioni->setCollezionista($this);
        }

        $this->impostazioni = $impostazioni;

        return $this;
    }
}
