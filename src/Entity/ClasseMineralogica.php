<?php

namespace App\Entity;

use App\Repository\ClasseMineralogicaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClasseMineralogicaRepository::class)
 */
class ClasseMineralogica
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $nome;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descrizione;

    /**
     * @ORM\OneToMany(targetEntity=Minerale::class, mappedBy="classe")
     */
    private $minerales;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $nomeMacchina;

    public function __construct()
    {
        $this->minerales = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id ? $this->nome : "";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(?string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getDescrizione(): ?string
    {
        return $this->descrizione;
    }

    public function setDescrizione(?string $descrizione): self
    {
        $this->descrizione = $descrizione;

        return $this;
    }

    /**
     * @return Collection|Minerale[]
     */
    public function getMinerales(): Collection
    {
        return $this->minerales;
    }

    public function addMinerale(Minerale $minerale): self
    {
        if (!$this->minerales->contains($minerale)) {
            $this->minerales[] = $minerale;
            $minerale->setClasse($this);
        }

        return $this;
    }

    public function removeMinerale(Minerale $minerale): self
    {
        if ($this->minerales->removeElement($minerale)) {
            // set the owning side to null (unless already changed)
            if ($minerale->getClasse() === $this) {
                $minerale->setClasse(null);
            }
        }

        return $this;
    }

    public function getNomeMacchina(): ?string
    {
        return $this->nomeMacchina;
    }

    public function setNomeMacchina(?string $nomeMacchina): self
    {
        $this->nomeMacchina = $nomeMacchina;

        return $this;
    }
}
