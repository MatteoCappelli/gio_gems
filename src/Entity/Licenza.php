<?php

namespace App\Entity;

use App\Repository\LicenzaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LicenzaRepository::class)
 */
class Licenza
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $db;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dataAcquisto;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dataScadenza;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2, nullable=true)
     */
    private $costo;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id ? $this->username : "";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getDb(): ?string
    {
        return $this->db;
    }

    public function setDb(?string $db): self
    {
        $this->db = $db;

        return $this;
    }

    public function getDataAcquisto(): ?\DateTimeInterface
    {
        return $this->dataAcquisto;
    }

    public function setDataAcquisto(?\DateTimeInterface $dataAcquisto): self
    {
        $this->dataAcquisto = $dataAcquisto;

        return $this;
    }

    public function getDataScadenza(): ?\DateTimeInterface
    {
        return $this->dataScadenza;
    }

    public function setDataScadenza(?\DateTimeInterface $dataScadenza): self
    {
        $this->dataScadenza = $dataScadenza;

        return $this;
    }

    public function getCosto(): ?string
    {
        return $this->costo;
    }

    public function setCosto(?string $costo): self
    {
        $this->costo = $costo;

        return $this;
    }
}
