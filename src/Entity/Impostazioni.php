<?php

namespace App\Entity;

use App\Repository\ImpostazioniRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ImpostazioniRepository::class)
 */
class Impostazioni
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $fontFamily;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $fontSize = 14;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $textAlign;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $backgroundColor = '#FFFFFF';

    /**
     * @ORM\OneToOne(targetEntity=Collezionista::class, inversedBy="impostazioni", cascade={"persist", "remove"})
     */
    private $collezionista;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $coloreCaption;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $coloreIntestazione = '#FFFFFF';

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $sfondoIntestazione = '#4CAF50';

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $sfondoPari = '#F2F2F2';

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $sfondoDispari = '#FFFFFF';

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $coloreTesto = '#000';

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $nascondiValore;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $firmaCollezionista;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $indirizzoCollezionista;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id ? $this->collezionista->getFullName() : "";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFontFamily(): ?string
    {
        return $this->fontFamily;
    }

    public function setFontFamily(?string $fontFamily): self
    {
        $this->fontFamily = $fontFamily;

        return $this;
    }

    public function getFontSize(): ?int
    {
        return $this->fontSize;
    }

    public function setFontSize(?int $fontSize): self
    {
        $this->fontSize = $fontSize;

        return $this;
    }

    public function getTextAlign(): ?string
    {
        return $this->textAlign;
    }

    public function setTextAlign(?string $textAlign): self
    {
        $this->textAlign = $textAlign;

        return $this;
    }

    public function getBackgroundColor(): ?string
    {
        return $this->backgroundColor;
    }

    public function setBackgroundColor(?string $backgroundColor): self
    {
        $this->backgroundColor = $backgroundColor;

        return $this;
    }

    public function getCollezionista(): ?Collezionista
    {
        return $this->collezionista;
    }

    public function setCollezionista(?Collezionista $collezionista): self
    {
        $this->collezionista = $collezionista;

        return $this;
    }

    public function getColoreCaption(): ?string
    {
        return $this->coloreCaption;
    }

    public function setColoreCaption(?string $coloreCaption): self
    {
        $this->coloreCaption = $coloreCaption;

        return $this;
    }

    public function getColoreIntestazione(): ?string
    {
        return $this->coloreIntestazione;
    }

    public function setColoreIntestazione(?string $coloreIntestazione): self
    {
        $this->coloreIntestazione = $coloreIntestazione;

        return $this;
    }

    public function getSfondoIntestazione(): ?string
    {
        return $this->sfondoIntestazione;
    }

    public function setSfondoIntestazione(?string $sfondoIntestazione): self
    {
        $this->sfondoIntestazione = $sfondoIntestazione;

        return $this;
    }

    public function getSfondoPari(): ?string
    {
        return $this->sfondoPari;
    }

    public function setSfondoPari(?string $sfondoPari): self
    {
        $this->sfondoPari = $sfondoPari;

        return $this;
    }

    public function getSfondoDispari(): ?string
    {
        return $this->sfondoDispari;
    }

    public function setSfondoDispari(?string $sfondoDispari): self
    {
        $this->sfondoDispari = $sfondoDispari;

        return $this;
    }

    public function getColoreTesto(): ?string
    {
        return $this->coloreTesto;
    }

    public function setColoreTesto(?string $coloreTesto): self
    {
        $this->coloreTesto = $coloreTesto;

        return $this;
    }

    public function getNascondiValore(): ?bool
    {
        return $this->nascondiValore;
    }

    public function setNascondiValore(?bool $nascondiValore): self
    {
        $this->nascondiValore = $nascondiValore;

        return $this;
    }

    public function getFirmaCollezionista(): ?string
    {
        return $this->firmaCollezionista;
    }

    public function setFirmaCollezionista(?string $firmaCollezionista): self
    {
        $this->firmaCollezionista = $firmaCollezionista;

        return $this;
    }

    public function getIndirizzoCollezionista(): ?string
    {
        return $this->indirizzoCollezionista;
    }

    public function setIndirizzoCollezionista(?string $indirizzoCollezionista): self
    {
        $this->indirizzoCollezionista = $indirizzoCollezionista;

        return $this;
    }
}
