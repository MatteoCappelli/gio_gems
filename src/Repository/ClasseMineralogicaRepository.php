<?php

namespace App\Repository;

use App\Entity\ClasseMineralogica;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ClasseMineralogica|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClasseMineralogica|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClasseMineralogica[]    findAll()
 * @method ClasseMineralogica[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClasseMineralogicaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClasseMineralogica::class);
    }

    // /**
    //  * @return ClasseMineralogica[] Returns an array of ClasseMineralogica objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ClasseMineralogica
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
