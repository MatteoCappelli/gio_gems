<?php

namespace App\Repository;

use App\Entity\Minerale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Minerale|null find($id, $lockMode = null, $lockVersion = null)
 * @method Minerale|null findOneBy(array $criteria, array $orderBy = null)
 * @method Minerale[]    findAll()
 * @method Minerale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MineraleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Minerale::class);
    }

    // /**
    //  * @return Minerale[] Returns an array of Minerale objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Minerale
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
