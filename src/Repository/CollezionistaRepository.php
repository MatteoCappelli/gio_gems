<?php

namespace App\Repository;

use App\Entity\Collezionista;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Collezionista|null find($id, $lockMode = null, $lockVersion = null)
 * @method Collezionista|null findOneBy(array $criteria, array $orderBy = null)
 * @method Collezionista[]    findAll()
 * @method Collezionista[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CollezionistaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Collezionista::class);
    }

    // /**
    //  * @return Collezionista[] Returns an array of Collezionista objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Collezionista
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
