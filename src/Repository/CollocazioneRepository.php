<?php

namespace App\Repository;

use App\Entity\Collocazione;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Collocazione|null find($id, $lockMode = null, $lockVersion = null)
 * @method Collocazione|null findOneBy(array $criteria, array $orderBy = null)
 * @method Collocazione[]    findAll()
 * @method Collocazione[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CollocazioneRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Collocazione::class);
    }

    // /**
    //  * @return Collocazione[] Returns an array of Collocazione objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Collocazione
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
