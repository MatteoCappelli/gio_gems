<?php

namespace App\Repository;

use App\Entity\Licenza;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Licenza|null find($id, $lockMode = null, $lockVersion = null)
 * @method Licenza|null findOneBy(array $criteria, array $orderBy = null)
 * @method Licenza[]    findAll()
 * @method Licenza[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LicenzaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Licenza::class);
    }

    // /**
    //  * @return Licenza[] Returns an array of Licenza objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Licenza
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
