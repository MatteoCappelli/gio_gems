<?php

namespace App\EventSubscriber;

use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security;

use Doctrine\ORM\EntityManagerInterface;

use App\Entity\User;
use App\Entity\Collezionista;
use App\Entity\Minerale;
use App\Entity\ClasseMineralogica;
use App\Entity\Impostazioni;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Security
     */
    private $security;

    public function __construct(EntityManagerInterface $entityManager, Security $security)
    {
        $this->em = $entityManager;
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            AfterEntityPersistedEvent::class => ['postPersist'],
            BeforeEntityPersistedEvent::class => ['prePersist'],
            BeforeEntityUpdatedEvent::class => ['preUpdate'],
        ];
    }

    public function postPersist(AfterEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        switch (true) {
          case $entity instanceof User:
            $this->nuovoCollezionista($entity);
            break;
          default:
            return;
        }

    }

    public function prePersist(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        switch (true) {
          case $entity instanceof ClasseMineralogica:
            $this->setMachineName($entity);
            break;
          case $entity instanceof Minerale:
            $this->setDataAndCollezionista($entity);
            break;
          case $entity instanceof Impostazioni:
            $this->setCollezionista($entity);
            break;
          default:
            return;
        }

    }

    public function preUpdate(BeforeEntityUpdatedEvent $event)
    {
        $entity = $event->getEntityInstance();

        switch (true) {
          case $entity instanceof ClasseMineralogica:
            $this->setMachineName($entity);
            break;
          default:
            return;
        }
    }

    public function setCollezionista($impostazioni)
    {
        $collezionista = $this->security->getUser()->getCollezionista();

        $impostazioni->setCollezionista($collezionista);

    }

    public function setMachineName($classe)
    {
      $nomeClasse = trim($classe->getNome());

      $nomeMacchina = \strtolower(
                            \str_replace([' ','\''], '_',
                                \str_replace([',','.','-',';',':', '"', '(', ')'], '', $nomeClasse)
                            )
                      );

      $classe->setNomeMacchina($nomeMacchina);

    }

    public function setDataAndCollezionista($minerale)
    {
        $collezionista = $this->security->getUser()->getCollezionista();
        $today = new \DateTime();

        $minerale->setCollezionista($collezionista);
        $minerale->setData($today);

    }

    public function nuovoCollezionista($user)
    {
        $collezionista = new Collezionista();

        $collezionista->setNome($user->getNome());
        $collezionista->setCognome($user->getCognome());
        $collezionista->setUser($user);

        $this->em->persist($collezionista);
        $this->em->flush();

    }

}


?>
