<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ColorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CurrencyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\HiddenField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;

use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;

use App\Form\Type\DocumentType;

use App\Entity\Minerale;
use App\Entity\Collezionista;

class MineraleCrudController extends AbstractCrudController
{

    public static function getEntityFqcn(): string
    {
        return Minerale::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Minerale')
            ->setEntityLabelInPlural('Minerali')
            ->setPageTitle('index', 'Elenco dei minerali')
            ->setPageTitle('new', 'Aggiungi un nuovo minerale')
            ->setEntityPermission('ROLE_COLLEZIONISTA')
            ->addFormTheme('form/custom_types.html.twig');
    }

    public function configureAssets(Assets $assets): Assets
    {
        return $assets
            ->addJsFile('js/google_autocomplete_minerale.js')
            ->addJsFile('js/document.js');
    }

    public function configureActions(Actions $actions): Actions
    {
        $generaPdf = Action::new('stampaPdf', 'Genera PDF', 'fas fa-file-pdf')
            ->linkToRoute('stampa_pdf', function (Minerale $minerale) {
                return [
                    'uuid' => $minerale->getId(),
                ];
            })
            ->setHtmlAttributes(['target' => '_blank']);

        $tornaAllaHome = Action::new('tornaAllaHome', 'Torna alla homepage', 'fa-fw fa fa-home')
            ->linkToRoute('admin', function() {
              return [
                'menuIndex' => 0,
                'submenuIndex' => -1,
              ];
            })
            ->setCssClass('btn btn-primary');

        return $actions
            ->remove(Crud::PAGE_DETAIL, Action::DELETE)
            ->remove(Crud::PAGE_DETAIL, Action::EDIT)
            ->remove(Crud::PAGE_DETAIL, Action::INDEX)
            ->add(Crud::PAGE_INDEX, $generaPdf)
            ->add(Crud::PAGE_DETAIL, $tornaAllaHome)
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                        return $action->setIcon('fas fa-plus')->setLabel('Aggiungi minerale');
                    })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                        return $action->setIcon('fas fa-plus-circle')->setLabel('Salva e aggiungine un altro');
                    })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                        return $action->setIcon('fas fa-chevron-circle-left')->setLabel('Salva e torna all\'elenco');
                    })
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                        return $action->setIcon('fas fa-plus')->setLabel('Aggiungi minerale');
                    })
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE, function (Action $action) {
                        return $action->setIcon('far fa-edit')->setLabel('Salva le modifiche e torna all\'elenco');
                    });

    }

    public function configureFields(string $pageName): iterable
    {
        $fluoChoices = [
          'Nessuna' => 0,
          'UV lw' => 1,
          'UV sw' => 2,
          'UV mw' => 3
        ];

        $fields_index = [
            Field::new('nome', 'Nome del minerale'),
            Field::new('numero', 'Scheda di appartenenza'),
            Field::new('condiviso', 'Condividere?'),
        ];

        $fields_detail = [
        FormField::addPanel('Informazioni del minerale')
            ->setIcon('fas fa-info-circle')
            ->addCssClass('col-6'),

            Field::new('nome', 'Nome del minerale'),
            Field::new('formula', 'Formula chimica del minerale'),
            Field::new('classe.nome', 'Classe mineralogica'),
            Field::new('accessori', 'Accessori'),

        FormField::addPanel('Ritrovamento del minerale')
            ->setIcon('fas fa-map-marked-alt')
            ->addCssClass('col-6'),

            Field::new('geoLoc', 'Luogo del ritrovamento'),
            Field::new('geoProv', 'Provincia di ritrovamento'),
            Field::new('geoReg', 'Regione di ritrovamento'),
            Field::new('geoNaz', 'Nazione di ritrovamento'),

        FormField::addPanel('Caratteristiche del minerale')
            ->setIcon('fab fa-unity')
            ->addCssClass('col-12'),

            Field::new('dimensione', 'Dimensioni del minerale'),
            Field::new('dimensioneCristallo', 'Dimensioni del cristallo maggiore'),
            Field::new('matrice', 'Tipo matrice'),
            Field::new('peso', 'Peso del minerale')
              ->setHelp('In grammi'),
            colorField::new('colore', 'Colore del minerale'),
            // Field::new('collocazione', 'Collocazione del minerale'),
            // Field::new('fluorescenza', 'Fluorescenza'),
            Field::new('fluoDescrizione', 'Descrizione fluorescenza'),

        FormField::addPanel('Valore del minerale')
            ->setIcon('fas fa-search-dollar')
            ->addCssClass('col-12'),

            MoneyField::new('valore', 'Valore del minerale')->setCurrency('EUR'),
            MoneyField::new('prezzoVendita', 'Prezzo di vendita del minerale')->setCurrency('EUR'),
            DateField::new('dataValore', 'Data di aggiornamento valore'),
            Field::new('note', 'Note aggiuntive'),

        FormField::addPanel('Allegati minerale')
            ->setIcon('fas fa-images')
            ->addCssClass('col-12'),

            CollectionField::new('documents', 'Allegati del minerale')
              ->setTemplatePath('admin/fields/documents.html.twig'),
        ];

        $fields_new_edit = [
          FormField::addPanel('Informazioni del minerale')
              ->setIcon('fas fa-info-circle')
              ->addCssClass('col-12'),

            TextField::new('nome', 'Nome del minerale')
              ->setRequired(false)
              ->addCssClass('col-4 full-width'),

            TextField::new('numero', 'Numero della scheda di appartenenza')
              ->setRequired(false)
              ->addCssClass('col-4 full-width'),

            TextField::new('formula', 'Formula chimica del minerale')
                ->setRequired(false)
                ->addCssClass('col-4 full-width'),

            TextareaField::new('accessori', 'Accessori')
                ->setRequired(false)
                ->addCssClass('col-8 full-width'),

            AssociationField::new('classe', 'Classe mineralogica')
                ->setRequired(false)
                ->addCssClass('col-4 full-width'),

          FormField::addPanel('Ritrovamento del minerale')
              ->setIcon('fas fa-map-marked-alt')
              ->addCssClass('col-4'),

              TextField::new('geoLoc', 'Luogo del ritrovamento')
                ->setRequired(false)
                ->addCssClass('col-12 full-width'),

              TextField::new('geoProv', 'Provincia di ritrovamento')
                ->setRequired(false)
                ->addCssClass('col-12 full-width'),

              TextField::new('geoReg', 'Regione di ritrovamento')
                ->setRequired(false)
                ->addCssClass('col-12 full-width'),

              TextField::new('geoNaz', 'Nazione di ritrovamento')
                ->setRequired(false)
                ->addCssClass('col-12 full-width'),

          FormField::addPanel('Caratteristiche del minerale')
              ->setIcon('fab fa-unity')
              ->addCssClass('col-8'),

              TextField::new('dimensione', 'Dimensioni del minerale')
                ->setRequired(false)
                ->addCssClass('col-4 full-width'),

              TextField::new('dimensioneCristallo', 'Dimensioni del cristallo maggiore')
                ->setRequired(false)
                ->addCssClass('col-4 full-width'),

              TextField::new('matrice', 'Tipo di matrice')
                ->setRequired(false)
                ->addCssClass('col-4 full-width'),

              TextField::new('peso', 'Peso del minerale')
                ->setHelp('In grammi')
                ->setRequired(false)
                ->addCssClass('col-4 full-width'),

              ColorField::new('colore', 'Colore del minerale')
                ->setRequired(false)
                ->addCssClass('col-4 full-width'),

              AssociationField::new('collocazione', 'Collocazione del minerale')
                ->setRequired(false)
                ->addCssClass('col-4 full-width'),

              ChoiceField::new('fluo', 'Fluorescenza')
                ->setRequired(false)
                ->addCssClass('col-4 full-width')
                ->setChoices($fluoChoices),

              TextareaField::new('fluoDescrizione', 'Descrizione fluorescenza')
                  ->setRequired(false)
                  ->addCssClass('col-8 full-width'),

          FormField::addPanel('Valore del minerale')
              ->setIcon('fas fa-search-dollar')
              ->addCssClass('col-8'),

              MoneyField::new('valore', 'Valore del minerale')
                ->setCurrency('EUR')
                ->setRequired(false)
                ->addCssClass('col-4 full-width'),

              MoneyField::new('prezzoVendita', 'Prezzo di vendita del minerale')
                ->setCurrency('EUR')
                ->setRequired(false)
                ->addCssClass('col-4 full-width'),

              DateField::new('valoreData', 'Data di aggiornamento valore')
                ->setRequired(false)
                ->addCssClass('col-4 full-width'),

              TextareaField::new('note', 'Note aggiuntive')
                  ->setRequired(false)
                  ->addCssClass('col-12 full-width'),

          FormField::addPanel('Gestione della collezione')
              ->setIcon('fas fa-tags')
              ->addCssClass('col-4'),

              DateField::new('dataCollezione', 'Data di ingresso nella collezione')
                ->setRequired(false)
                ->addCssClass('col-12 full-width'),

              BooleanField::new('condiviso', 'Condividere?')
                  ->setRequired(false)
                  ->addCssClass('col-12 full-width'),

          FormField::addPanel('Allegati minerale')
              ->setHelp('Il primo allegato inserito avrà più risalto rispetto agli altri sia nel PDF che in homepage.')
              ->setIcon('fas fa-images')
              ->addCssClass('col-12'),

              CollectionField::new('documents')->onlyOnForms()
                ->allowAdd()
                ->allowDelete()
                ->setEntryIsComplex(false)
                ->setEntryType(DocumentType::class)
                ->setFormTypeOptions([
                    'by_reference' => 'false',
                    'label' => false,
                    'attr' => [
                        'class' => 'row no-gutters' // Horizontal layout
                    ]
                ]),

        ];

        switch ($pageName) {
          case Crud::PAGE_INDEX: // list
            return $fields_index;
            break;
          case Crud::PAGE_NEW: // new
            return $fields_new_edit;
            break;
          case Crud::PAGE_EDIT: // edit
            return $fields_new_edit;
            break;
          case Crud::PAGE_DETAIL: // show
            return $fields_detail;
            break;
        }

    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters);

        $qb = $this->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);

        if (!$this->isGranted('ROLE_SUPER_ADMIN')) {

            $collezionista = $this->getUser()->getCollezionista();

            $qb->where('entity.collezionista = '.$collezionista->getId());

        }

        return $qb;
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entity): void
    {
        try {
            if (count($entity->getDocuments()) > 0) {
              $this->validateDocuments($entity);
            }
           $this->addFlash('success', 'Minerale modificato con successo!');
           parent::updateEntity($entityManager, $entity);
        } catch (\Exception $e) {
           $this->addFlash('danger', $e->getMessage());
        }
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entity): void
    {
        try {
            if (count($entity->getDocuments()) > 0) {
              $this->validateDocuments($entity);
            }
           $this->addFlash('success', 'Minerale creato con successo!');
           parent::persistEntity($entityManager, $entity);
        } catch (\Exception $e) {
           $this->addFlash('danger', $e->getMessage());
        }
    }

    public function validateDocuments($entity)
    {

      foreach ($entity->getDocuments() as $document) {
        $document->setMinerale($entity);
      }

    }
}
