<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;

use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Config\Definition\Exception\Exception;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;

use App\Entity\User;

class UserCrudController extends AbstractCrudController
{

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Utente')
            ->setEntityLabelInPlural('Utenti')
            ->setPageTitle('index', 'Elenco utenti')
            ->setPageTitle('new', 'Aggiungi utente')
            ->setEntityPermission('ROLE_SUPER_ADMIN');
    }

    public function configureFields(string $pageName): iterable
    {
        $rolesChoices = [
          'Collezionista' => 'ROLE_COLLEZIONISTA',
          'Amministratore' => 'ROLE_SUPER_ADMIN'
        ];

        $fields_index = [
            Field::new('getFullName', 'Nominativo'),
            Field::new('rolesStr', 'Ruolo')
            ->formatValue(function ($role) {
                return $role === 'ROLE_SUPER_ADMIN' ? 'Amministratore' : 'Collezionista';
            }),
            Field::new('username'),
            Field::new('email'),
            Field::new('enabled', 'Attivo?'),
        ];

        $fields_new_edit = [
          FormField::addPanel('Informazioni utente')
              ->setIcon('fas fa-user')
              ->addCssClass('col-8'),

            TextField::new('nome')
              ->setRequired(true)
              ->addCssClass('col-6 full-width'),

            TextField::new('cognome')
              ->setRequired(true)
              ->addCssClass('col-6 full-width'),

            ChoiceField::new('rolesStr', 'Ruolo')
              ->setRequired(true)
              ->addCssClass('col-6 full-width')
              ->renderExpanded(true)
              ->setChoices($rolesChoices),

            BooleanField::new('enabled', 'Account attivo?')
              ->setRequired(false)
              ->addCssClass('col-6 full-width'),

          FormField::addPanel('Credenziali di accesso')
              ->setIcon('fas fa-user-lock')
              ->addCssClass('col-4 full-width'),

            TextField::new('username', 'Username')
                ->setRequired(true)
                ->addCssClass('col-12 full-width'),

            EmailField::new('email')
              ->setRequired(true)
              ->addCssClass('col-12 full-width'),

            TextField::new('plainPassword', 'Password')
                ->setHelp('Testo semplice')
                ->setRequired(true)
                ->addCssClass('col-12 full-width')
                ->hideOnIndex(),

        ];

        switch ($pageName) {
          case Crud::PAGE_INDEX: // list
            return $fields_index;
            break;
          case Crud::PAGE_NEW: // new
            return $fields_new_edit;
            break;
          case Crud::PAGE_EDIT: // edit
            return $fields_new_edit;
            break;
          case Crud::PAGE_DETAIL: // show
            break;
        }

    }

    public function updateEntity(EntityManagerInterface $entityManager, $entity): void
    {
        try {
           $this->addFlash('success', 'Utente modificato con successo!');
           parent::updateEntity($entityManager, $entity);
        } catch (\Exception $e) {
           $this->addFlash('danger', $e->getMessage());
        }
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entity): void
    {
        try {
           $this->addFlash('success', 'Utente creato con successo!');
           parent::persistEntity($entityManager, $entity);
        } catch (\Exception $e) {
           $this->addFlash('danger', $e->getMessage());
        }
    }
    //
    // public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    // {
    //     parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters);
    //
    //     $qb = $this->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
    //     if ($this->isGranted('ROLE_SUPERADMIN')) {
    //     } else if ($this->isGranted('ROLE_ADMIN')) {
    //         $user = $this->getUser();
    //         $qb->where('entity.id = '.$user->getId()." OR entity.roles LIKE '%ROLE_INCARICATO%'");
    //     } else {
    //         $user = $this->getUser();
    //         $qb->where('entity.id = '.$user->getId());
    //     }
    //     return $qb;
    // }
}
