<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

use Doctrine\ORM\EntityManagerInterface;

use App\Entity\User;
use App\Entity\Minerale;
use App\Entity\Licenza;
use App\Entity\ClasseMineralogica;
use App\Entity\Collocazione;
use App\Entity\Impostazioni;

class DashboardController extends AbstractDashboardController
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $minerali = $this->em->getRepository(Minerale::class)->findByCondiviso(true);

        return $this->render('admin/homepage.html.twig', [
          'minerali' => $minerali
        ]);
    }

    public function configureAssets(): Assets
    {
        return Assets::new()
          ->addCssFile('css/system/easy_admin.css')
          ->addCssFile('css/admin/admin.css')
          ->addCssFile('css/admin/homepage.css');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
          ->setTitle('<img src="/system/images/gio_gems.jpg">')
          ->setFaviconPath('/system/images/favicon.ico');
    }

    // public function configureActions(): Actions
    // {
    //     return $actions
    //         ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
    //             return $action->setLabel('Salva e crea un altro');
    //           });
    // }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linkToDashboard('Home', 'fa fa-home'),

            MenuItem::section('Gestione minerali')
                      ->setPermission('ROLE_COLLEZIONISTA'),
            MenuItem::linkToCrud('Minerali', 'far fa-gem', Minerale::class)
                      ->setPermission('ROLE_COLLEZIONISTA'),
            MenuItem::linkToCrud('Classi mineralogiche', 'fas fa-tags', ClasseMineralogica::class)
                      ->setPermission('ROLE_SUPER_ADMIN'),
            MenuItem::linkToCrud('Collocazioni', 'fas fa-map-marker-alt', Collocazione::class)
                      ->setPermission('ROLE_SUPER_ADMIN'),

            MenuItem::section('Preferenze di sistema')
                      ->setPermission('ROLE_COLLEZIONISTA'),
            MenuItem::linkToRoute('Preferenze stampa PDF', 'fas fa-cogs', 'preferenze_pdf')
                      ->setPermission('ROLE_COLLEZIONISTA'),

            MenuItem::section('Gestione utenza')
                      ->setPermission('ROLE_SUPER_ADMIN'),
            MenuItem::linkToCrud('Utenti', 'fa fa-users', User::class)
                      ->setPermission('ROLE_SUPER_ADMIN'),

            MenuItem::section('Amministrazione')
                      ->setPermission('ROLE_SUPER_ADMIN'),
            MenuItem::linkToCrud('Licenze', 'fas fa-address-card', Licenza::class)
                      ->setPermission('ROLE_SUPER_ADMIN'),

        ];
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        return parent::configureUserMenu($user)
            ->setName($user->getFullName());
    }
}
