<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ColorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CurrencyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\HiddenField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;

use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Licenza;

class LicenzaCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Licenza::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Licenza')
            ->setEntityLabelInPlural('Licenze')
            ->setPageTitle('index', 'Elenco licenze')
            ->setPageTitle('new', 'Aggiungi licenza')
            ->setEntityPermission('ROLE_SUPER_ADMIN');
    }

    public function configureFields(string $pageName): iterable
    {

        $fields_index = [
            Field::new('url'),
            Field::new('db')
        ];

        $fields_new_edit = [
          FormField::addPanel('Informazioni sulla licenza')
              ->setIcon('fas fa-info-circle')
              ->addCssClass('col-12'),

            TextField::new('username')
              ->setRequired(false)
              ->addCssClass('col-12 full-width'),

            TextField::new('url')
                ->setRequired(false)
                ->addCssClass('col-12 full-width'),

            TextField::new('db')
                ->setRequired(false)
                ->addCssClass('col-12 full-width'),

            DateField::new('dataAcquisto')
                ->setRequired(false)
                ->addCssClass('col-12 full-width'),

            DateField::new('dataScadenza')
                ->setRequired(false)
                ->addCssClass('col-12 full-width'),

            MoneyField::new('costo')
              ->setCurrency('EUR')
              ->setRequired(false)
              ->addCssClass('col-12 full-width'),

        ];

        switch ($pageName) {
          case Crud::PAGE_INDEX: // list
            return $fields_index;
            break;
          case Crud::PAGE_NEW: // new
            return $fields_new_edit;
            break;
          case Crud::PAGE_EDIT: // edit
            return $fields_new_edit;
            break;
          case Crud::PAGE_DETAIL: // show
            break;
        }

    }

    public function updateEntity(EntityManagerInterface $entityManager, $entity): void
    {
        try {
           $this->addFlash('success', 'Licenza modificata con successo!');
           parent::updateEntity($entityManager, $entity);
        } catch (\Exception $e) {
           $this->addFlash('danger', $e->getMessage());
        }
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entity): void
    {
        try {
           $this->addFlash('success', 'Licenza creata con successo!');
           parent::persistEntity($entityManager, $entity);
        } catch (\Exception $e) {
           $this->addFlash('danger', $e->getMessage());
        }
    }
}
