<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ColorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CurrencyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\HiddenField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;

use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;

use App\Entity\ClasseMineralogica;

class ClasseMineralogicaCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ClasseMineralogica::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Classe mineralogica')
            ->setEntityLabelInPlural('Classe mineralogiche')
            ->setPageTitle('index', 'Elenco delle classi mineralogiche')
            ->setPageTitle('new', 'Aggiungi classe mineralogica')
            ->setEntityPermission('ROLE_SUPER_ADMIN');
    }

    public function configureFields(string $pageName): iterable
    {

        $fields_index = [
            Field::new('nome', 'Nome della classe mineralogica'),
            Field::new('descrizione', 'Descrizione della classe mineralogica di appartenenza')
        ];

        $fields_new_edit = [
          FormField::addPanel('Informazioni sulla classe mineralogica')
              ->setIcon('fas fa-tag')
              ->addCssClass('col-12'),

            TextField::new('nome', 'Nome della classe mineralogica')
              ->setRequired(false)
              ->addCssClass('col-4 full-width'),

            TextareaField::new('descrizione', 'Descrizione della classe mineralogica')
                ->setRequired(false)
                ->addCssClass('col-8 full-width'),

        ];

        switch ($pageName) {
          case Crud::PAGE_INDEX: // list
            return $fields_index;
            break;
          case Crud::PAGE_NEW: // new
            return $fields_new_edit;
            break;
          case Crud::PAGE_EDIT: // edit
            return $fields_new_edit;
            break;
          case Crud::PAGE_DETAIL: // show
            break;
        }

    }

    public function updateEntity(EntityManagerInterface $entityManager, $entity): void
    {
        try {
           $this->addFlash('success', 'Classe mineralogica modificata con successo!');
           parent::updateEntity($entityManager, $entity);
        } catch (\Exception $e) {
           $this->addFlash('danger', $e->getMessage());
        }
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entity): void
    {
        try {
           $this->addFlash('success', 'Classe mineralogica creata con successo!');
           parent::persistEntity($entityManager, $entity);
        } catch (\Exception $e) {
           $this->addFlash('danger', $e->getMessage());
        }
    }

}
