<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ColorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CurrencyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\HiddenField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;

use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Impostazioni;

class ImpostazioniCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Impostazioni::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Preferenze stampa PDF')
            ->setEntityLabelInPlural('Preferenze stampa PDF')
            ->setPageTitle('new', 'Stampa PDF')
            ->setPageTitle('edit', 'Stampa PDF')
            ->setEntityPermission('ROLE_COLLEZIONISTA');
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_NEW, Action::SAVE_AND_CONTINUE)

            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN)
            ->remove(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER)
            ->remove(Crud::PAGE_NEW, Action::SAVE_AND_RETURN)

            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE, function (Action $action) {
                return $action->setLabel('Salva le modifiche');
              })
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_CONTINUE, function (Action $action) {
                return $action->setIcon('fas fa-check')->setLabel('Imposta come preferenze');
              });

    }

    public function configureFields(string $pageName): iterable
    {

        // $alignChoices = [
        //   'Giustificato' => 'justify',
        //   'Centrato' => 'center',
        //   'A destra' => 'right',
        //   'A sinistra' => 'left'
        // ];

        $fields_index = [
            Field::new('fontFamily', 'Nome del font'),
            Field::new('fontSize', 'Dimensioni del font'),
            // TextField::new('textAlign', 'Allineamento del testo')
            //   ->formatValue(function ($align) {
            //     switch ($align) {
            //       case 'justify':
            //         return 'Giustificato';
            //         break;
            //       case 'center':
            //         return 'Centrato';
            //         break;
            //       case 'right':
            //         return 'A destra';
            //         break;
            //       case 'left':
            //         return 'A sinistra';
            //         break;
            //     }
            //   }),
            Field::new('backgroundColor', 'Colore dello sfondo')
              ->setTemplatePath('admin/fields/background_pdf_field.html.twig'),
        ];

        $fields_new_edit = [

        FormField::addPanel('Corpo del PDF ')
            ->setHelp('Imposta tutto ciò che riguarda il font nel PDF.')
            ->setIcon('fas fa-file-pdf')
            ->addCssClass('col-12'),

          TextField::new('fontFamily', 'Nome del font')
            ->setFormType(TextType::class)
            ->setFormTypeOptions([
              'attr' => [
                'placeholder' => 'Esempi: Montserrat, Roboto, Arial, ecc...'
              ]
            ])
            ->setHelp('Inserire solo il NOME del font da utilizzare.')
            ->setRequired(false)
            ->addCssClass('col-6 full-width'),

          IntegerField::new('fontSize', 'Dimensioni del font')
              ->setHelp('È consigliato utilizzare un valore non troppo grande né troppo piccolo.')
              ->setFormType(IntegerType::class)
              ->setFormTypeOptions([
                'attr' => [
                  'min' => 0
                ],
                'empty_data' => 14
              ])
              ->setRequired(false)
              ->addCssClass('col-6 full-width'),

          ColorField::new('coloreTesto', 'Colore font')
              ->setHelp('Colore principale del carattere.')
              ->setFormType(ColorType::class)
              ->setFormTypeOptions([
                'empty_data' => '#FFFFFF'
              ])
              ->setRequired(false)
              ->addCssClass('col-4 full-width'),

          ColorField::new('coloreCaption', 'Colore caption')
              ->setHelp('Colore dei caption sopra le tabelle.')
              ->setFormType(ColorType::class)
              ->setFormTypeOptions([
                'empty_data' => '#FFFFFF'
              ])
              ->setRequired(false)
              ->addCssClass('col-4 full-width'),

          ColorField::new('coloreIntestazione', 'Colore titoli')
              ->setHelp('Colore del carattere intestazione tabelle.')
              ->setFormType(ColorType::class)
              ->setFormTypeOptions([
                'empty_data' => '#FFFFFF'
              ])
              ->setRequired(false)
              ->addCssClass('col-4 full-width'),

          FormField::addPanel('Firma del PDF')
              ->setHelp('Imposta il footer del PDF e scegli se mostrare la tabella "Valore del minerale".')
              ->setIcon('fas fa-file-signature')
              ->addCssClass('col-12'),

            TextField::new('indirizzoCollezionista', 'Indirizzo collezionista')
              ->setRequired(false)
              ->addCssClass('col-6 full-width'),

            BooleanField::new('nascondiValore', 'Nascondere tabella "Valore del minerale"?')
                ->setRequired(false)
                ->addCssClass('col-6 full-width'),

          FormField::addPanel('Colori di sfondo')
              ->setHelp('Scegli i vari colori di sfondo.')
              ->setIcon('fas fa-palette')
              ->addCssClass('col-12'),

            // ChoiceField::new('textAlign', 'Allineamento del testo')
            //     ->setFormType(ChoiceType::class)
            //     ->setFormTypeOptions([
            //       'empty_data' => 'justify'
            //     ])
            //     ->setChoices($alignChoices)
            //     ->setRequired(false)
            //     ->addCssClass('col-6 full-width'),

            ColorField::new('backgroundColor', 'Colore sfondo')
                ->setHelp('Colore di sfondo del pdf.')
                ->setRequired(false)
                ->addCssClass('col-6 full-width'),

            ColorField::new('sfondoIntestazione', 'Sfondo intestazione')
                ->setHelp('Colore di sfondo intestazione tabelle.')
                ->setRequired(false)
                ->addCssClass('col-6 full-width'),

            ColorField::new('sfondoIntestazione', 'Sfondo intestazione')
                ->setHelp('Colore di sfondo intestazione tabelle.')
                ->setRequired(false)
                ->addCssClass('col-6 full-width'),

            ColorField::new('sfondoPari', 'Sfondo celle pari')
                ->setHelp('Colore di sfondo celle pari delle tabelle.')
                ->setRequired(false)
                ->addCssClass('col-6 full-width'),

            ColorField::new('sfondoDispari', 'Sfondo celle dispari')
                ->setHelp('Colore di sfondo celle dispari delle tabelle.')
                ->setRequired(false)
                ->addCssClass('col-6 full-width'),

        ];

        switch ($pageName) {
          case Crud::PAGE_INDEX: // list
            return $fields_index;
            break;
          case Crud::PAGE_NEW: // new
            return $fields_new_edit;
            break;
          case Crud::PAGE_EDIT: // edit
            return $fields_new_edit;
            break;
          case Crud::PAGE_DETAIL: // show
            break;
        }

    }

    public function updateEntity(EntityManagerInterface $entityManager, $entity): void
    {
        try {
           $this->addFlash('success', 'Preferenze per la stampa PDF modificate con successo!');
           parent::updateEntity($entityManager, $entity);
        } catch (\Exception $e) {
           $this->addFlash('danger', $e->getMessage());
        }
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entity): void
    {
        try {
           $this->addFlash('success', 'Preferenze per la stampa PDF impostate con successo!');
           parent::persistEntity($entityManager, $entity);
        } catch (\Exception $e) {
           $this->addFlash('danger', $e->getMessage());
        }
    }

}
