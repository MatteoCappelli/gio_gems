<?php

namespace App\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

use App\Controller\Admin\DashboardController;
use App\Controller\Admin\ImpostazioniCrudController;

class RouteController extends AbstractController
{
    private $crudUrlGenerator;

    public function __construct(CrudUrlGenerator $crudUrlGenerator)
    {
        $this->crudUrlGenerator = $crudUrlGenerator;
    }

    /**
     * @Route("/", name="app_index")
     */
    public function index(): Response
    {
        return new RedirectResponse('admin/');
    }

    /**
     * @Route("/preferenze-pdf", name="preferenze_pdf")
     */
    public function preferenzePdf()
    {
      $user = $this->getUser();
      $collezionista = $user = $this->getUser()->getCollezionista();

      if (is_null($collezionista->getImpostazioni())) {

        $url = $this->crudUrlGenerator
            ->build()
            ->setController(ImpostazioniCrudController::class)
            ->setAction(Action::NEW);

        return $this->redirect($url);

      } else {

        $url = $this->crudUrlGenerator
            ->build()
            ->setController(ImpostazioniCrudController::class)
            ->setAction(Action::EDIT)
            ->setEntityId($collezionista->getImpostazioni()->getId());

        return $this->redirect($url);

      }

    }

}
