<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpClient\HttpClient;

use Doctrine\ORM\EntityManagerInterface;

use Knp\Snappy\Pdf;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;

use App\Entity\Minerale;

class BackendController extends AbstractController
{
    protected $knpSnappy;

    protected $em;

    public function __construct(EntityManagerInterface $em, Pdf $knpSnappy)
    {
        $this->knpSnappy = $knpSnappy;
        $this->em = $em;
    }

    /**
     * @Route("/stampa-pdf", name="stampa_pdf")
     */
    public function stampaPdf(Request $request)
    {
        $mineraleId = $request->query->get('uuid');

        $minerale = $this->em->getRepository(Minerale::class)->findOneById($mineraleId);
        $impostazioni = $minerale->getCollezionista()->getImpostazioni() ? $minerale->getCollezionista()->getImpostazioni() : '';

        $html = $this->renderView(
            'pdf/minerale.html.twig', [
                'minerale' => $minerale,
                'impostazione' => $impostazioni,
                'base_path' => $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST']
            ]
        );

        return new Response(
            $this->knpSnappy->getOutputFromHtml($html),
            200,
            [
              'Content-Type' => 'application/pdf',
              'Content-Disposition' => 'inline; filename="' . $minerale->getNome() . '.pdf"'
            ]
        );

    }

}
?>
