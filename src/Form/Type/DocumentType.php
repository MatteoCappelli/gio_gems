<?php

namespace App\Form\Type;

use App\Entity\Document;
use App\Repository\DocumentRepository;
use DateTime;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Form\FormInterface as FormFormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\Test\FormInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DocumentType extends AbstractType
{
    private $params;
    private $docrep;

    public function __construct(ContainerBagInterface $params, DocumentRepository $docrep)
    {
        $this->params = $params;
        $this->docrep = $docrep;
    }

    private static function loadImg($path) {
        if (!file_exists($path)) {
            return null;
        }
        list($imgw, $imgh, $imgtype) = getimagesize($path);
        if (!$imgw || !$imgh || !$imgtype) {
            return null;
        }
        switch ($imgtype) {
        case 3: // why some png files return 3?
        case IMG_PNG:$img = imagecreatefrompng($path);
            break;
        case IMG_JPG:
        case IMG_JPEG:$img = imagecreatefromjpeg($path);
            break;
        case IMG_WEBP:$img = imagecreatefromwebp($path);
            break;
        }
        if (!$img) {
            return null;
        }
        return ['w' => $imgw, 'h' => $imgh, 'img' => $img];
    }

    private static function resizeImage($input, $outputPath, $options) {
        $options = array_merge([
            'max_width' => 1280,
            'max_height' => 1280,
            'fill_width' => 0,
            'fill_height' => 0,
            'fill_color' => 'ffffff',
            'enlarge_smaller' => false,
            'quality' => 75
        ], $options);

        if ($options['fill_width'] || $options['fill_height']) {
            if ($options['fill_width'] < $options['max_width']) {
                $options['fill_width'] = $options['max_width'];
            }
            if ($options['fill_height'] < $options['max_height']) {
                $options['fill_height'] = $options['max_height'];
            }
        }
        if (is_array($input)) {
            $inputImg = $input;
        } else {
            $inputImg = self::loadImg($input);
        }

        if ($inputImg['w'] >= $inputImg['h']) {
            if ($inputImg['w'] > $options['max_width'] || $options['enlarge_smaller']) {
                $new_img_width = $outW = $options['max_width'];
                $new_img_height = $outH = $inputImg['h'] * ($new_img_width / $inputImg['w']);
            } else {
                $new_img_width = $outW = $inputImg['w'];
                $new_img_height = $outH = $inputImg['h'];
            }
        } else {
            if ($inputImg['h'] > $options['max_height'] || $options['enlarge_smaller']) {
                $new_img_height = $outH = $options['max_height'];
                $new_img_width = $outW = $inputImg['w'] * ($new_img_height / $inputImg['h']);
            } else {
                $new_img_width = $outW = $inputImg['w'];
                $new_img_height = $outH = $inputImg['h'];
            }
        }

        if ($options['fill_width'] || $options['fill_height']) {
            $outW = $options['fill_width'];
            $outH = $options['fill_height'];
            $out_image = imagecreatetruecolor($outW, $outH);
            $bgcolor = hexdec($options['fill_color']);
            $img_bgcolor = imagecolorallocate($out_image, $bgcolor >> 16 & 255, $bgcolor >> 8 & 255, $bgcolor & 255);
            imagefilledrectangle($out_image, 0, 0, $outW - 1, $outH - 1, $img_bgcolor);
        } else {
            $out_image = imagecreatetruecolor($outW, $outH);
        }

        if ($inputImg['w'] != $new_img_width || $inputImg['h'] != $new_img_height) {
            imagecopyresampled($out_image, $inputImg['img'], round(($outW - $new_img_width)/2), round(($outH - $new_img_height)/2), 0, 0, $new_img_width, $new_img_height, $inputImg['w'], $inputImg['h']);
        } else {
            imagecopy($out_image, $inputImg['img'], round(($outW - $inputImg['w'])/2), round(($outH - $inputImg['h'])/2), 0, 0, $inputImg['w'], $inputImg['h']);
        }

        return imagejpeg($out_image, $outputPath, $options['quality']);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addViewTransformer(new CallbackTransformer(function ($data) {
            return $data;
        }, function ($file) use ($options) {

            $destDir = $this->params->get('kernel.project_dir').'/public/uploads';

            if (is_string($file) && ctype_digit($file)) {
                $doc = $this->docrep->find($file);
            } else {
                if ($file instanceof UploadedFile) {
                    $doc = new Document();
                    $doc->setMime($file->getMimeType());
                    $doc->setFname($file->getClientOriginalName());
                    $doc->setDateAdded(new DateTime());
                    if (substr($doc->getMime(),0,6) == 'image/') {
                        $doc->setType('image');
                    } else {
                        $doc->setType('doc');
                    }
                    $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

                    $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                    $newFilename = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

                    try {
                        $newFile = $file->move(
                            $destDir.'/'.$doc->getType(),
                            $newFilename
                        );
                    } catch (FileException $e) {
                        exit($e->getMessage());
                    }
                    $doc->setPath(substr($newFile->getPathName(), strlen($destDir)+1));
                } else {
                    $file = strtr($file, '-_','+/');
                    $res = preg_match('/^([^,]+),data:([^;]+);base64,(.*)$/', $file, $matches);
                    if (!$res) {
                        exit('DocumentType: unexpected data');
                    }
                    // TODO make mime and extension detection more secure (as with UploadedFile)
                    $fname = urldecode(base64_decode($matches[1]));
                    $t = strpos($fname,'.');
                    $mime = $matches[2];
                    $ext = $t === false? '' : substr($fname, $t);
                    $fdata = base64_decode($matches[3]);
                    $doc = new Document();
                    $doc->setMime($mime);
                    $doc->setFname($fname);
                    $doc->setDateAdded(new DateTime());
                    if (substr($doc->getMime(),0,6) == 'image/') {
                        $doc->setType('image');
                    } else {
                        $doc->setType('doc');
                    }
                    $originalFilename = pathinfo($fname, PATHINFO_FILENAME);

                    $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                    $newFilename = $safeFilename.'-'.uniqid().$ext;

                    $path = $destDir.'/'.$doc->getType().'/'.$newFilename;

                    try {
                        file_put_contents($path, $fdata);
                    } catch (FileException $e) {
                        exit($e->getMessage());
                    }
                    $doc->setPath(substr($path, strlen($destDir)+1));
                }
                if ($doc->getType() == 'image') {
                    $img = self::loadImg($destDir.'/'.$doc->getPath());
                    if ((isset($options['image_resize']['fill_width']) && $options['image_resize']['fill_width'] && $img['w'] != $options['image_resize']['fill_width']) ||
                        (isset($options['image_resize']['fill_height']) && $options['image_resize']['fill_height'] && $img['h'] != $options['image_resize']['fill_height']) ||
                        ((!isset($options['image_resize']['fill_width']) || !$options['image_resize']['fill_width']) && $img['w'] != $options['image_resize']['max_width']) ||
                        ((!isset($options['image_resize']['fill_height']) || !$options['image_resize']['fill_height']) && $img['w'] != $options['image_resize']['max_height'])) {
                        self::resizeImage($img, $destDir.'/'.$doc->getPath(), $options['image_resize']);
                    }
                    $pi = pathinfo($destDir.'/'.$doc->getPath());
                    if (!file_exists($pi['dirname'].'/.thumbs')) {
                        mkdir($pi['dirname'].'/.thumbs');
                    }
                    self::resizeImage($img, $pi['dirname'].'/.thumbs/'.$pi['basename'], $options['thumb_resize']);
                }
            }
            return $doc;
        }));
    }

    public function buildView(FormView $view, FormFormInterface $form, array $options)
    {
        $view->vars['image_resize'] = $options['image_resize'];
    }

    public function finishView(FormView $view, FormFormInterface $form, array $options)
    {
        $view->vars['multipart'] = true;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'compound' => false,
            'multiple' => false,
            'allow_file_upload' => true,
            'data_class' => Document::class,
            'required' => false,
            'block_prefix' => 'collection_entry',
            'image_resize' => [
                'max_width' => 1280,
                'max_height' => 1280
            ],
            'thumb_resize' => [
                'max_width' => 320,
                'max_height' => 320
            ],
            'row_attr' => [
                 'class' => 'document-collection-entry'
            ]
        ]);
    }

}
