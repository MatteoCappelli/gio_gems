<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegistrationType extends AbstractType
{
   public function buildForm(FormBuilderInterface $builder, array $options)
   {
      $builder
        ->add('nome', null, [
          'label' => false,
          'attr' => [
            'class' => 'form-control',
            'placeholder' => 'Nome',
            'required' => true
          ]
        ])
        ->add('cognome', null, [
          'label' => false,
          'attr' => [
            'class' => 'form-control',
            'placeholder' => 'Cognome',
            'required' => true
          ]
        ])
        ->add('email', null, [
          'label' => false,
          'attr' => [
            'class' => 'form-control',
            'placeholder' => 'Email'
          ]
        ])
        ->add('plainPassword', null, array(
          'label' => false,
          'attr' => [
            'class' => 'form-control',
            'placeholder' => 'Password',
            'autocomplete' => 'off'
          ]
        ))
        ->remove('username')
      ;
   }

   public function getParent()
   {
      return 'FOS\UserBundle\Form\Type\RegistrationFormType';
   }

   public function getBlockPrefix()
   {
      return 'app_user_registration';
   }

   public function getName()
   {
      return $this->getBlockPrefix();
   }

}
