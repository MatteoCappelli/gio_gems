if (typeof App == 'undefined') App = {};
App.document = {
    def: function (opts, defs) {
        for (var k in defs) {
            if (opts[k] === undefined) opts[k] = defs[k];
        }
    },
    resizeImage: function (file_input, opts, preview, hidden) {
        App.document.def(opts, {
            'max_width': 1280,
            'max_height': 1280,
            'fill_width': 0,
            'fill_height': 0,
            'fill_color': 'ffffff',
            'enlarge_smaller' : false,
            'quality': .75
        });

        if (opts.fill_width || opts.fill_height) {
            if (opts.fill_width < opts.max_width) {
                opts.fill_width = opts.max_width;
            }
            if (opts.fill_height < opts.max_height) {
                opts.fill_height = opts.max_height;
            }
        }

        var fileName = file_input.files[0].name;
        var reader = new FileReader();

        reader.onload = function (event) {
            var img = new Image();
            img.onload = function () {
                var elem = document.createElement('canvas');
                var new_img_width, new_img_height;
                if (img.width >= img.height) {
                    if (img.width > opts.max_width || opts.enlarge_smaller) {
                        new_img_width = elem.width = opts.max_width;
                        new_img_height = elem.height = img.height * (new_img_width / img.width);
                    } else {
                        new_img_width = elem.width = img.width;
                        new_img_height = elem.height = img.height;
                    }
                } else {
                    if (img.height > opts.max_height || opts.enlarge_smaller) {
                        new_img_height = elem.height = opts.max_height;
                        new_img_width = elem.width = img.width * (new_img_height / img.height);
                    } else {
                        new_img_width = elem.width = img.width;
                        new_img_height = elem.height = img.height;
                    }
                }
                var ctx = elem.getContext('2d');
                if (opts.fill_width || opts.fill_height) {
                    elem.width = opts.fill_width;
                    elem.height = opts.fill_height;
                    ctx.fillStyle = '#' + opts.fill_color;
                    ctx.fillRect(0, 0, elem.width, elem.height);
                }
                ctx.drawImage(img, (elem.width - new_img_width) / 2, (elem.height - new_img_height) / 2, new_img_width, new_img_height);
                var dataurl = elem.toDataURL(file_input.files[0].type, opts.quality);
                if (preview) {
                    if ($(preview).is('img')) {
                        $(preview).attr('src', dataurl);
                    } else {
                        $(preview).css('background-image', dataurl);
                    }
                }
                if (hidden) {
                    $(hidden).val((btoa(encodeURIComponent(fileName))+','+dataurl).replace(/\+/g,'-').replace(/\//g,'_'));
                } else {
                    ctx.canvas.toBlob((blob) => {
                        var file = new File([blob], fileName, {
                            type: file_input.files[0].type,
                            lastModified: Date.now()
                        });
                        $(file_input).data('file', file);
                    }, file_input.files[0].type, opts.quality);
                }
            }
            img.src = event.target.result;
        };
        reader.onerror = function (error) {
            console.log(error);
        };
        reader.readAsDataURL(file_input.files[0]);
    },
    init : function () {
        $('form').on('change', '.document-file', function () {
            var fileName = this.value.replace('C:\\fakepath\\', '');
            if (!fileName) { // Chrome fix
                return;
            }
            var ext = fileName.substr(fileName.lastIndexOf('.')+1).toLowerCase();
            $(this).parent().find('.document-fname').attr('title', fileName).text(fileName);
            var preview = $(this).parent().find('.document-preview');
            var hidden = $(this).parent().find('input[type="hidden"]');
            hidden.val('').prop('disabled', true);
            if (this.files[0].type.substr(0,6) == 'image/') {
                preview.attr('src','').removeClass('document-doc').addClass('document-img');
                App.document.resizeImage(this, $(this).closest('.document-widget').data('image-resize'), preview, hidden);
            } else {
                preview.attr('src','').removeClass('document-img').addClass('document-doc');
                preview.attr('src', "images/doc-icon.svg");
            }
        }).on('submit', function () {
            $('input.document-file[type="file"]', this).each(function () {
                var hidden = $(this).parent().find('input[type="hidden"]');
                if (hidden && hidden.val()) {
                    $(this).prop('disabled', true);
                    hidden.prop('disabled', false);
                }
            });
        });
    }
};
$(document).ready(function () {
    App.document.init();
});
