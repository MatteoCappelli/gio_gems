
  function googleAutocompleteRitrovamento() {
    var input = document.getElementById('Minerale_geoLoc');
    var autocomplete = new google.maps.places.Autocomplete(input);
      google.maps.event.addListener(autocomplete, 'place_changed', function () {
          var place = autocomplete.getPlace();
          var address_components = place.address_components;
          var components = {};

          $.each(address_components, function(k,v1) {jQuery.each(v1.types, function(k2, v2){components[v2]=v1.long_name});});

           document.getElementById('Minerale_geoProv').value = components.administrative_area_level_2;
           document.getElementById('Minerale_geoNaz').value = components.country;
           document.getElementById('Minerale_geoReg').value = components.administrative_area_level_1;

      });
  }

  google.maps.event.addDomListener(window, 'load', googleAutocompleteRitrovamento);
